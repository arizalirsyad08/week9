package week9;

public class Main
{
    public static void main(String[] args)
    {
        BangunDatar bd = new BangunDatar(2);
        System.out.println("------ LUAS --------");
        System.out.println("luas persegi : "+bd.luas(2));
        System.out.println("luas persegi panjang : "+bd.luas(2,4));
        System.out.println("luas segitiga : "+bd.luas(2.0,4.0));
        System.out.println("luas lingkaran : "+bd.luas(10.0));

        BangunRuang kb = new BangunRuang(2,2,2);
        BangunRuang bl = new BangunRuang(2,3,4);
        BangunRuang tb = new BangunRuang(2,2,2);
        BangunRuang kr = new BangunRuang(2,2,2);
        System.out.println("------ VOLUME --------");
        System.out.println("volume kubus : "+kb.volume(2,2,2));
        System.out.println("volume balok : "+bl.volume(2,3,4));
        System.out.println("volume tabung : "+tb.volume(2.0,4));
        System.out.println("volume kerucut : "+tb.volume(2,4));
    }
}