package week9;

public class BangunRuang extends BangunDatar
{
    private int tinggi;
    private static final double pi = 3.14;

    public BangunRuang(int panjang, int lebar, int tinggi)
    {
        super(panjang,lebar);
        this.tinggi = tinggi;
    }

    public BangunRuang(int sisi)
    {
        super(sisi);
        this.sisi = sisi;
    }

    public int volume(int sisi)
    {
        return super.getSisi()*super.getSisi()*super.getSisi();
    }

    public int volume(int panjang, int lebar, int tinggi)
    {
        return super.getPanjang()*super.getLebar()*tinggi;
    } 

    public double volume(double a, int tinggi)
    {
        return pi*a*a*tinggi;
    }

    public double volume(int a, int tinggi)
    {
        return (pi*a*a*tinggi)/3;
    }


}